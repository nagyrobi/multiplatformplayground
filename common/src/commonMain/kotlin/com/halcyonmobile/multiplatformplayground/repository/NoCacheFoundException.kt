package com.halcyonmobile.multiplatformplayground.repository

/**
 * Exception thrown by the MemorySources if it can't find the requested data.
 */
class NoCacheFoundException : Throwable()