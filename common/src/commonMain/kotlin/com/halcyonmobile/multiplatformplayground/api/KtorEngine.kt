package com.halcyonmobile.multiplatformplayground.api

import io.ktor.client.engine.HttpClientEngine

internal expect val engine: HttpClientEngine