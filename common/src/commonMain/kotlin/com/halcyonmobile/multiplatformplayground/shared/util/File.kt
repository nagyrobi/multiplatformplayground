package com.halcyonmobile.multiplatformplayground.shared.util

expect class File

expect fun File.toByteArray(): ByteArray
