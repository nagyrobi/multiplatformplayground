rootProject.name="MultiplatformPlayground"
include(":app",":backend", ":common", "commonModel")

enableFeaturePreview("GRADLE_METADATA")